//
//  SourceControlXcode12App.swift
//  SourceControlXcode12
//
//  Created by Xinghang Li on 2021-06-23.
//

import SwiftUI

@main
struct SourceControlXcode12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
